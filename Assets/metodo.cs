﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metodo : MonoBehaviour
{

    public GameObject game;
    // Start is called before the first frame update
    //void Start()
    //{
    //    suma(2,4);
    //}

    // Update is called once per frame
    //void Update()
    //{
    //    suma(2, 4);
    //}


    void suma(int a, int b)
    {
        int sum = a + b;
        Debug.Log("la suma de los numeros es : "+ sum);
    }

    private void OnEnable()
    {
        //DesactivarComponente();
    }

    private void Start()
    {
        DesactivarRender();
    }

    void DesactivarComponente()
    {
        gameObject.GetComponent<start_awake>().enabled = false;
    }

    void DesactivarRender()
    {
        game.GetComponent<MeshRenderer>().enabled = false;
        
    }
}
