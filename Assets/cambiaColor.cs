﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiaColor : MonoBehaviour
{
    public GameObject game;

    public bool estado;

    private void Update()
    {
        if (estado)
        {
            Verde();
        }
        else
        {
            Rojo();
        }
        
    }

    void Verde()
    {
        game.GetComponent<MeshRenderer>().material.color = Color.green;
    }
    void Rojo()
    {
        game.GetComponent<MeshRenderer>().material.color = Color.red;
    }
}
